<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Finansur</title>
        <link rel="stylesheet" href="css/index.css">
        <meta charset="UTF-8">
    </head>
    <body>
        <header>
            <img src="../img/logo.png">
            <h1>Finansur</h1>
            <h2>Solidez y Confianza</h2>
            <nav>
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Consultar</a>
                        <ul>
                            <li><a href="">Cotizacion del Bitcoin</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
        <section class="cotizaciones">
            <table>
                <tr>
                    <th>Proveedor</th>
                    <th>Fecha</th>
                    <th>Moneda</th>
                    <th>Cotiacion</th>
                </tr>
                <c:forEach items="${requestScope.cotizaciones}" var="item">
                    <c:choose>
                        <c:when test="${item.getPrecio()<=0}">
                            <tr>
                                <td>${item.getProveedor()}</td>
                                <td>El proveedor no se encuentra disponible.</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td>${item.getProveedor()}</td>
                                <td><fmt:formatDate value="${item.getFecha()}" /></td>
                                <td>${item.getMoneda()}</td>
                                <td>${item.getPrecio()}</td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </table>
        </section>
        <footer>
            <p>Finansur</p>
        </footer>
    </body>
</html>