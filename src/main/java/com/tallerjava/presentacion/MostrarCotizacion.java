package com.tallerjava.presentacion;

import com.tallerjava.modelo.Cotizacion;
import com.tallerjava.repository.BinanceCotizacionRepository;
import com.tallerjava.repository.CoinDeskCotizacionRepository;
import com.tallerjava.repository.CotizacionRepository;
import com.tallerjava.repository.CryptoCotizacionRepository;
import com.tallerjava.repository.FinanSurCotizacionRepository;
import com.tallerjava.repository.SomosPNTCotizacionRepository;
import com.tallerjava.service.CotizacionService;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MostrarCotizacion", urlPatterns = {"/MostrarCotizacion"})
public class MostrarCotizacion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<CotizacionRepository> repositorios = new ArrayList();
        repositorios.add(new CoinDeskCotizacionRepository());
        repositorios.add(new BinanceCotizacionRepository());
        repositorios.add(new SomosPNTCotizacionRepository());
        repositorios.add(new CryptoCotizacionRepository());
        repositorios.add(new FinanSurCotizacionRepository());
        Date date = new Date();
        SimpleDateFormat zonaHoraria = new SimpleDateFormat("d-M-yyyy hh:mm");
        CotizacionService service = new CotizacionService(repositorios);
        List<Cotizacion> cotizaciones = service.obtenerCotizaciones();
        
        request.setAttribute("cotizaciones", cotizaciones);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
