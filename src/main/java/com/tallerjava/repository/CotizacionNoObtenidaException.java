package com.tallerjava.repository;

public class CotizacionNoObtenidaException extends RuntimeException {

    public CotizacionNoObtenidaException() {
        super();
    }

    public CotizacionNoObtenidaException(String msg, Throwable causa) {
        super(msg, causa);
    }

    public CotizacionNoObtenidaException(String msg) {
        super(msg);
    }
    
    public CotizacionNoObtenidaException(Throwable causa) {
        super(causa);
    }
}
