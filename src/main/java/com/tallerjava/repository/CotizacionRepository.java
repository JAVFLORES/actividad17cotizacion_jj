package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;

public abstract class CotizacionRepository {

    public abstract Cotizacion obtenerCotizacion();

    public abstract String getNombre();
}
