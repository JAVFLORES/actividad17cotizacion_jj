package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class BackupCotizacion {

    private String usuario = "Taller";
    private String password = "1234";

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Cotizacion recuperarCotizacion(String proveedor) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/finansur", usuario, password); Statement statement = conexion.createStatement()) {
                ResultSet result = statement.executeQuery(
                        "select proveedor, fecha_cotizacion, moneda, precio "
                        + "from cotizacion "
                        + "where proveedor = '" + proveedor + "' "
                        + "order by fecha_cotizacion desc;");
                if (result.next()) {
                    return new Cotizacion(result.getString("proveedor"), result.getTimestamp("fecha_cotizacion"), result.getString("moneda"), result.getDouble("precio"));

                } else {
                    throw new CotizacionNoRecuperadaException("No hay cotizaciones");
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new CotizacionNoRecuperadaException("No se pudo recuperar la cotizacion", ex);
        }
    }

    public void guardarCotizacion(Cotizacion cotizacion) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            SimpleDateFormat zonaHoraria = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try (Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/finansur", usuario, password); Statement statement = conexion.createStatement()) {
                statement.executeUpdate("INSERT INTO COTIZACION "
                        + "(proveedor, fecha_cotizacion, moneda, precio) values('"
                        + cotizacion.getProveedor() + "', '"
                        + zonaHoraria.format(cotizacion.getFecha()) + "', '"
                        + cotizacion.getMoneda() + "', '"
                        + cotizacion.getPrecio() + "');");
                statement.close();
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new CotizacionNoGuardadaException("No se puedo guardar la cotizacion", ex.getCause());
        }
    }
}
